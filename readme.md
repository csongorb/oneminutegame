# 1minutegame

A short (aprox. one minute) game by [**csongor baranyai**](http://www.csongorb.com) for Android.

- [1minutegame on Google Play Store](https://play.google.com/store/apps/details?id=com.csongorb.oneminutegame)
- [1minutegame on itch.io](https://csongorb.itch.io/oneminutegame)

Made with:

- [Processing](http://processing.org) 3.3.6
    - [Android Mode](http://android.processing.org) 4.0
- [Open Sans](https://www.fontsquirrel.com/fonts/open-sans)
- Android Asset Studio: [Launcher Icon Generator](https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html)

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

## Changelog

### Bugs

- multitouch support (don't end input, when touching with a second finger)

### Ideas

- a more responsive design for tablets
- something to play with during the 1 minute
    - NOT blurring / masking / etc.
    - [https://processing.org/examples/follow3.html](https://processing.org/examples/follow3.html)
- version for BBC micro:bit
- multiplayer

### 0.21 - Arrows & Permissions (6. November 2017)

- adjusted bar- & arrow-display-behaviour for better / clearer interface
- proper permission-management at runtime
    - as requested by higher target SDK versions (23 or higher)
    - [https://developer.android.com/training/permissions/requesting.html](https://developer.android.com/training/permissions/requesting.html)
    - NOT optimized for changing the permissions DURING runtime
    - blue flash indicating no permissions, thus no saving

### 0.2 - Android (4. November 2017)

- first Android version
- releases
    - Google Play Store
    - itch.io

### 0.1 - Desktop (2. September 2016)

- first (slightly) public version
- desktop / OSX
