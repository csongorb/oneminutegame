float a = 0.0;
float inc = TWO_PI/25.0;

int nr = 1000;
color[] c = new color[nr];
int[] l = new int[nr];

float[] f = new float[nr];
float[] y = new float[nr];

void setup() {
  size (200, 500);

  for (int i = 0; i < nr; i++) {
    c[i] = color(random(255), random(255), random(255));
    l[i] = int(random(width/3, width - 1));
  }
}

void draw() {

  background(0);
  noStroke();

  int size = height / nr;

  for (int i = 0; i < nr; i++) {
    f[i] = sin(radians(lerp(271, 90, float(i)/float(nr))));
    f[i] = f[i] + 1;
    f[i] = pow(f[i], nr/10) + 0.5;
    if (i == 0) {
      y[i] = 0;
    } else {
      y[i] = y[i-1] + f[i];
    }
  }
  
  float factor = float(height) / (y[nr - 1] + f[nr - 1]);

  for (int i = 0; i < nr; i++) {
    fill(c[i]);
    //rect(0, i * size, l[i], size);
    rect(0, y[i] * factor, l[i], f[i] * factor);
  }

  for (int i = 0; i < 100; i=i+4) {
    if (degrees(a) > 90 && degrees(a) < 270) {
      stroke(255);
      line(i, 50, i, 25+sin(a)*20.0);
    }
    a = a + inc;
  }
}