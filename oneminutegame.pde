
ArrayList<OneGame> myGames;

int goalT = 60000;
int minT = 3000;

boolean isPressed = false;
int pressedAt;
int pressedAtX, pressedAtY;

PFont myFont, myFontBold;

float displayW = 1.33; // relation between goalTime and screenWidth on screen
float tempW;
float gSpeed;

PImage screenImg;
PGraphics titleMask;

int actTime;
int lastStep;
int lastTime;

PrintWriter output;
BufferedReader reader;

GameLogic myGame;

// Debug
boolean debugOn = false;
boolean loadFakeSavegames = true;
boolean showFrameRate = false;

void setup() {

  //size(300, 600);
  fullScreen();
  orientation(PORTRAIT);
  noSmooth();

  myFont = createFont("open-sans/OpenSans-Regular.ttf", 16 * displayDensity);
  myFontBold = createFont("open-sans/OpenSans-ExtraBold.ttf", 16 * displayDensity);

  screenImg = createGraphics(width, height);

  titleMask = createGraphics(width, height);
  createTitleMask();

  // request permissions only if there are not already granted
  if (!permissionsGranted()) {
    requestStoragePermissions();
  }

  myGame = new GameLogic(goalT, minT);
}

void draw() {

  background(0);

  updateTime();

  myGame.drawMain();
  if (isPressed) {
    noStroke();
    fill(80);
    ellipse(pressedAtX, pressedAtY, width*1.5, width*1.5);
  }

  if (debugOn) {
    if (showFrameRate) {
      fill(255, 0, 0);
      textAlign(RIGHT, BOTTOM);
      textFont(myFont, 8 * displayDensity);
      text(frameRate, width - (4 * displayDensity), height - (4 * displayDensity));
    }
  }
}

void touchStarted() {
  myGame.isPressed();
}

void touchEnded() {
  myGame.isReleased();
}

void updateTime() {
  actTime = millis();
  lastStep = actTime - lastTime;
  lastTime = actTime;
}

void createTitleMask() {  

  titleMask.beginDraw();
  titleMask.noStroke();
  titleMask.textAlign(CENTER, CENTER);

  // HOLD
  titleMask.fill(220);
  titleMask.textFont(myFontBold, 72 * displayDensity);
  titleMask.text("HOLD", width/2, height/2 - (10 * displayDensity));

  // for a minute
  titleMask.textFont(myFont, 16 * displayDensity);
  titleMask.text("for a minute", width/2, height/2 + (35 * displayDensity));

  // credits
  titleMask.textAlign(LEFT, BOTTOM);
  titleMask.textFont(myFont, 8 * displayDensity);
  titleMask.text("csongorb.com", 4 * displayDensity, height - (4 * displayDensity));

  titleMask.endDraw();
}

void requestStoragePermissions() {  
  requestPermission("android.permission.READ_EXTERNAL_STORAGE", "initReadingPermission");
  requestPermission("android.permission.WRITE_EXTERNAL_STORAGE", "initWritingPermission");
}

boolean permissionsGranted() {
  boolean r = false;
  if (hasPermission("android.permission.READ_EXTERNAL_STORAGE")) {
    if (hasPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
      r = true;
    }
  }
  return r;
}

void initReadingPermission(boolean granted) {
  if (granted) {
    //println("init reading permission");
  } else {
    //println("reading permission is not available");
  }
}

void initWritingPermission(boolean granted) {
  if (granted) {
    //println("init writing permission");
  } else {
    //println("writing permission is not available");
  }
}