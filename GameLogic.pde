
class GameLogic {

  // GameLogic
  // everything regarding the main game mechanic
  // not so much about the design

  int hToyID;
  int tempX, tempY;

  int minLastHeight;

  int goalTime;
  int minTime;

  GameLogic(int _goalTime, int _minTime) {
    goalTime = _goalTime;
    minTime = _minTime;

    minLastHeight = height / 12;
    gSpeed = width / float(2); // pixels per second

    SavegameLoad();
  }

  void update() {
  }

  void drawMain() {
    background(0);
    noStroke();

    fill(50);
    rect(0, 0, map(width, 0, width * displayW, 0, width), height);

    if (myGames.size() > 0) {

      float hTemp;
      hTemp = height / float(myGames.size());
      if (hTemp < minLastHeight) {
        hTemp = (height - minLastHeight) / (float(myGames.size()) - 1.0);
      }

      float firstH;
      if (hTemp < minLastHeight) {
        firstH = minLastHeight;
      } else {
        firstH = hTemp;
      }

      for (int i = 0; i < myGames.size(); i++) {

        float yTemp = height - (height / float(myGames.size()) * float(i));
        if (hTemp < minLastHeight) {
          yTemp = height - ((height - minLastHeight) / (float(myGames.size()) - 1.0) * float(i));
        }

        OneGame thisGame = myGames.get(i); 
        if (i == myGames.size() - 1) {
          if (myGames.size() == 1) {
            // draw FIRST game
            thisGame.draw(0, firstH, color(200), true, true);
          } else {
            // draw NEWEST game
            thisGame.draw(0, firstH, color(200), true, false);
          }
        } else {
          // draw all OLDER games
          thisGame.draw(yTemp - hTemp, hTemp, color(120), false, false);
        }
      }
    }

    image(screenImg, 0, 0);
    image(titleMask, 0, 0);

    // unfortunatelly BLENDing and BLURing is so slow on Android
    // there is no chance I can do that
  }

  void isPressed() {
    if (!isPressed) {
      isPressed = true;
      pressedAt = millis();

      pressedAtX = int(touches[0].x);
      pressedAtY = int(touches[0].y);
    }
  }

  void isReleased() {
    if (isPressed) {
      int tTime = millis() - pressedAt;
      if (tTime >= minTime) {
        myGames.add(new OneGame(goalTime, tTime));
        myGame.SavegameSave();
        tempW = 0;
      } else {
        background(255, 0, 0);
        tempW = width;
      }
    }
    isPressed = false;
  }

  void SavegameSave() {

    // save data to SD-card

    // attention! 
    // you have to grand permission to the app on your device
    // see below (SamegameLoad) for further details!!! 
    
    if (permissionsGranted()) {

      output = createWriter("//sdcard//oneminutegame/save.txt");
      for (int i = 0; i < myGames.size(); i++) {
        OneGame thisGame = myGames.get(i);
        output.print(thisGame.time);
        if (i != myGames.size()-1) {
          output.print("/");
        }
      }
      output.flush();
      output.close();
    } else {

      // without permissions: blue flash

      fill(0, 0, 255);
      noStroke();
      rect(0, 0, width, height);
    }
  }

  void SavegameLoad() {

    // load data from SD-card
    // if there is no existing file, create a new file!

    // attention! 
    // as this is (for sure) the first time the app tries to access the SD-card...
    // ...android.permissions are checked!

    // saveStrings & loadStrings ARE NOT really working on android!!!

    if (permissionsGranted()) {

      String line;

      // createReader
      //println(savefileExists());
      if (!savefileExists()) {
        createSavefile();
      }
      reader = createReader("//sdcard//oneminutegame/save.txt");

      // get the content of the file
      try {
        line = reader.readLine();
      }
      catch (IOException e) {
        line = null;
      }

      if (debugOn) {
        if (loadFakeSavegames) {
          // short
          //line = "10367/67422/43628/35750/15951/59790/27048/29085/34372/122689/35413/2183/3152/4034/3349/22953/59093/1285/42241/63392/73960/45022/68992";
          // long
          line = "71616/23886/36269/12614/8315/7443/3251/9985/5765/5932/8326/1110/60012/7821/9438/14871/31595/12437/49301/10367/67422/43628/35750/15951/59790/27048/29085/34372/122689/35413/2183/3152/4034/3349/22953/59093/1285/42241/63392/73960/45022";
        }
      }

      // if saveFile HAS NO content: create empty list
      if (line == null) {
        myGames = new ArrayList<OneGame>();
      } else {

        // if saveFile HAS content: read it
        String[] pieces = split(line, "/");
        myGames = new ArrayList<OneGame>();
        for (int i = 0; i < pieces.length; i++) {
          myGames.add(new OneGame(goalTime, int(pieces[i])));
        }
      }

      tempW = width;
    } else {

      // if permissions are not granted
      myGames = new ArrayList<OneGame>();
    }
  }

  boolean savefileExists() {
    boolean fileThere = false;
    if ((new File("//sdcard//oneminutegame/save.txt")).exists()) {
      fileThere = true;
    }
    return fileThere;
  }

  void createSavefile() {
    output = createWriter("//sdcard//oneminutegame/save.txt");
    output.flush();
    output.close();
  }
}