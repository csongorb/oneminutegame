
class OneGame {

  int time;
  int goal;

  int missedBy;
  float w;
  boolean textOn;
  boolean hasGrown;

  OneGame(int _goal, int _time) {
    time = _time;
    goal = _goal;

    missedBy = time - goal;
    w = map(time, 0, goal * displayW, 0, width); // width of the bar
    textOn = false;
    hasGrown = false;
  }

  void draw(float _y, float _height, color _c, boolean _last, boolean _first) {

    noStroke();
    fill(_c);

    // draw LAST time (let it grow first)
    if (_last) {
      tempW = tempW + (gSpeed * (lastStep/float(1000)));
      if (tempW >= w || tempW >= width) {
        tempW = w;
        textOn = true;
        hasGrown = true;
      }
      rect(0, _y, tempW, _height);
    } else {
      // draw ALL OTHER times
      rect(0, _y, w, _height);
      textOn = false;
      hasGrown = true;
    }
    
    // draw ARROW if time too long for width
    if (time > goal * displayW) {
      if (hasGrown) {
        fill(50);
        beginShape();
        float arrowWidth = _height/1.5;
        if (arrowWidth > (width - (width / displayW))){
          arrowWidth = width - (width / displayW);
        }
        vertex(width - arrowWidth, _y);
        vertex(width, _y + (_height/2));
        vertex(width - arrowWidth, _y + _height);
        endShape();
      }
    }

    // display TEXT
    if (textOn) {

      float textY = _y + (_height/2);
      if (_first) {
        textY = height/4;
      }

      float missedF = missedBy * 0.001;
      String xs = nf(missedF, 0, 2) + "s";
      if (missedF > 0) {
        xs = "+" + xs;
      }

      float goalF = 0.3;
      if (missedF <= goalF && missedF >= -goalF) {
        xs = "YES! (" + xs + ")";
      }

      textFont(myFont, 16 * displayDensity);

      // display text ON the time bar
      if (time >= goal / 3 * 2) {
        fill(0);
        if (time > goal * displayW) {
          textAlign(CENTER, CENTER);
          text(xs, width/2, textY);
        } else {
          textAlign(RIGHT, CENTER);
          text(xs, map(time, 0, goal * displayW, 0, width) - (15 * displayDensity), textY);
        }
      } else {
        // display text BESIDES the time bar
        fill(200);
        textAlign(LEFT, CENTER);
        text(xs, map(time, 0, goal * displayW, 0, width) + (15 * displayDensity), textY);
      }
    }
  }
}